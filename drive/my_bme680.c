#include "my.h"
#define BME_IIC_ADDR   (0xee)
struct bme680_dev            gas_sensor;
struct bme680_field_data     filed_struct;
struct bme680_calib_data     calib_struct;
struct bme680_tph_sett       tph_sett_struct;
struct bme680_gas_sett       gas_sett_struct;
struct bme680_field_data     field_data_struct;
// int8_t my_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len);
// int8_t my_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len);
// void my_delay(uint32_t period);


#define BME680_W_SELF_TEST_FAILED 3
#define MIN_TEMPERATURE INT16_C(0)		/* 0 degree Celsius */
#define MAX_TEMPERATURE INT16_C(6000) 	/* 60 degree Celsius */

#define MIN_PRESSURE UINT32_C(90000)	/* 900 hecto Pascals */
#define MAX_PRESSURE UINT32_C(110000) 	/* 1100 hecto Pascals */

#define MIN_HUMIDITY UINT32_C(20000)	/* 20% relative humidity */
#define MAX_HUMIDITY UINT32_C(80000) 	/* 80% relative humidity*/

#define HEATR_DUR	2000
#define N_MEAS		6
#define LOW_TEMP	150
#define HIGH_TEMP 	350

bsec_bme_settings_t bsec_bme_settings;


void init_bme680(void)
{
    int rslt;
    uint8_t set_required_settings;
    gas_sensor.read  =  my_i2c_read;
    gas_sensor.write =  my_i2c_write;
    gas_sensor.delay_ms = my_delay;

    gas_sensor.intf              = BME680_I2C_INTF; //interface : IIC
    gas_sensor.power_mode        = BME680_FORCED_MODE; //模式选择
    gas_sensor.chip_id           = BME680_CHIP_ID;    //芯片id
    gas_sensor.dev_id            = BME680_I2C_ADDR_SECONDARY;

    gas_sensor.calib             = calib_struct;    
    gas_sensor.tph_sett          = tph_sett_struct;
    gas_sensor.gas_sett          = gas_sett_struct;

    gas_sensor.tph_sett.os_hum   = BME680_OS_1X;     
    gas_sensor.tph_sett.os_temp  = BME680_OS_1X;
    gas_sensor.tph_sett.os_pres  = BME680_OS_1X;
    gas_sensor.tph_sett.filter   = BME680_FILTER_SIZE_1;

   gas_sensor.power_mode = BME680_FORCED_MODE;
  /* Set the required sensor settings needed */ 
    set_required_settings = (BME680_OST_SEL | BME680_OSP_SEL | BME680_OSH_SEL | BME680_FILTER_SEL| BME680_GAS_SENSOR_SEL);
    rslt =  bme680_init(&gas_sensor);             
    if(rslt == BME680_OK)
    {
        printf(" init done \r\n");
    }else {
        printf(" init failed rslt = %d\r\n",rslt);
    }

    /* Set the desired sensor configuration */
	 rslt =  bme680_set_sensor_settings(set_required_settings,&gas_sensor);
   if(rslt)printf("result of bme680_set_sensor_settings() is %d",rslt);

	/* Set the power mode */
	rslt =  bme680_set_sensor_mode(&gas_sensor);   
    if(rslt)printf("result of bme680_set_sensor_mode() is %d",rslt);
    //bsec_init();
}

void read_bme(void)
{ 
    uint8_t set_required_settings;
    uint8_t rslt;
    uint16_t meas_period;
    rslt =  bme680_set_sensor_mode(&gas_sensor);
    if(rslt)printf("result of bme680_set_sensor_mode() is %d",rslt);  
    bme680_get_profile_dur(&meas_period,&gas_sensor);  
    my_delay(meas_period*2);           
    bme680_get_sensor_data(&field_data_struct, &gas_sensor); 
    printf("temp     : %d \r\n",field_data_struct.temperature);
    printf("humi     : %d \r\n",field_data_struct.humidity);
    printf("pressure : %d \r\n",field_data_struct.pressure);
}


bsec_library_return_t bsec_sensor_control(const int64_t time_stamp, bsec_bme_settings_t *sensor_settings);

void test(void)
{
   bsec_library_return_t bsec_library_return;
   bsec_bme_settings.next_call = 0;

    int64_t next_call;                  /*!< @brief Time stamp of the next call of the sensor_control*/
    uint32_t process_data;              /*!< @brief Bit field describing which data is to be passed to bsec_do_steps() @sa BSEC_PROCESS_* */
    uint16_t heater_temperature;        /*!< @brief Heating temperature [degrees Celsius] */
    uint16_t heating_duration;          /*!< @brief Heating duration [ms] */
    uint8_t run_gas;                    /*!< @brief Enable gas measurements [0/1] */
    uint8_t pressure_oversampling;      /*!< @brief Pressure oversampling settings [0-5] */
    uint8_t temperature_oversampling;   /*!< @brief Temperature oversampling settings [0-5] */
    uint8_t humidity_oversampling;      /*!< @brief Humidity oversampling settings [0-5] */
    uint8_t trigger_measurement;        /*!< @brief Trigger a forced measurement with these settings now [0/1] */

bsec_library_return = bsec_sensor_control(time(),&bsec_bme_settings);



}
// static int8_t analyze_sensor_data(struct bme680_field_data *data, uint8_t n_meas)
// {
// 	int8_t rslt = BME680_OK;
// 	uint8_t self_test_failed = 0, i;
// 	uint32_t cent_res = 0;

// 	if ((data[0].temperature < MIN_TEMPERATURE) || (data[0].temperature > MAX_TEMPERATURE))
// 		self_test_failed++;

// 	if ((data[0].pressure < MIN_PRESSURE) || (data[0].pressure > MAX_PRESSURE))
// 		self_test_failed++;

// 	if ((data[0].humidity < MIN_HUMIDITY) || (data[0].humidity > MAX_HUMIDITY))
// 		self_test_failed++;

// 	for (i = 0; i < n_meas; i++) /* Every gas measurement should be valid */
// 		if (!(data[i].status & BME680_GASM_VALID_MSK))
// 			self_test_failed++;

// 	if (n_meas >= 6)
// 		cent_res = (data[3].gas_resistance + data[5].gas_resistance) / (2 * data[4].gas_resistance);

// 	if ((cent_res * 5) < 6)
// 		self_test_failed++;

// 	if (self_test_failed)
// 		rslt = BME680_W_SELF_TEST_FAILED;

// 	return rslt;
// }
void BME_test(struct bme680_dev *dev)
{  
    int8_t rslt = BME680_OK;
	struct bme680_field_data data[N_MEAS];

	struct bme680_dev t_dev;

	/* Copy required parameters from reference bme680_dev struct */
	t_dev.dev_id = dev->dev_id;
	t_dev.amb_temp = 25;
	t_dev.read = dev->read;
	t_dev.write = dev->write;
	t_dev.intf = dev->intf;
	t_dev.delay_ms = dev->delay_ms;

	rslt = bme680_init(&t_dev);

	// if (rslt == BME680_OK) {
	// 	/* Select the power mode */
	// 	/* Must be set before writing the sensor configuration */
	// 	t_dev.power_mode = BME680_FORCED_MODE;

	// 	uint16_t settings_sel;

	// 	/* Set the temperature, pressure and humidity & filter settings */
	// 	t_dev.tph_sett.os_hum = BME680_OS_1X;
	// 	t_dev.tph_sett.os_pres = BME680_OS_16X;
	// 	t_dev.tph_sett.os_temp = BME680_OS_2X;

	// 	/* Set the remaining gas sensor settings and link the heating profile */
	// 	t_dev.gas_sett.run_gas = BME680_ENABLE_GAS_MEAS;
	// 	t_dev.gas_sett.heatr_dur = HEATR_DUR;

	// 	settings_sel = BME680_OST_SEL | BME680_OSP_SEL | BME680_OSH_SEL | BME680_GAS_SENSOR_SEL;

	// 	uint16_t profile_dur = 0;
	// 	bme680_get_profile_dur(&profile_dur, &t_dev);

	// 	uint8_t i = 0;
	// 	while ((rslt == BME680_OK) && (i < N_MEAS)) {
	// 		if (rslt == BME680_OK) {

	// 			if (i % 2 == 0)
	// 				t_dev.gas_sett.heatr_temp = HIGH_TEMP; /* Higher temperature */
	// 			else
	// 				t_dev.gas_sett.heatr_temp = LOW_TEMP; /* Lower temperature */

	// 			rslt = bme680_set_sensor_settings(settings_sel, &t_dev);

	// 			if (rslt == BME680_OK) {

	// 				rslt = bme680_set_sensor_mode(&t_dev); /* Trigger a measurement */

	// 				t_dev.delay_ms(profile_dur); /* Wait for the measurement to complete */

	// 				rslt = bme680_get_sensor_data(&data[i], &t_dev);
                    
    //                 printf("temp     : %d \r\n",field_data_struct.temperature);
    //                 printf("humi     : %d \r\n",field_data_struct.humidity);
    //                 printf("pressure : %d \r\n",field_data_struct.pressure);
	// 			}
	// 		}

	// 		i++;
	// 	}

	// 	if (rslt == BME680_OK)
	// 		rslt = analyze_sensor_data(data, N_MEAS);
	// }
}




