#ifndef _MY_H_
#define _MY_H_

#include "bme680.h"
#include "my_i2c.h"
#include "bme680_defs.h"
#include <stdio.h>
#include "bsec_datatypes.h"
#include "bsec_interface.h"
#include <sys/time.h>

void init_bme680(void);
void BME_test(struct bme680_dev *dev);
void read_bme(void);
void test(void);
#endif


