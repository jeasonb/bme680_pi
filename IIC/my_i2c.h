#ifndef _MY_IIC_H_
#define _MY_IIC_H_

int8_t my_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len);
int8_t my_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len);
void my_delay(uint32_t period);

#endif


