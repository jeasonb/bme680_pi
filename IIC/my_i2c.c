
#include <bcm2835.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include "my_i2c.h"

#define MAX_LEN 16

int8_t my_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
    uint8_t regOffset=0;
    char  read_addr[10] = {0};
    bcm2835_i2c_setSlaveAddress(dev_id);    
    
    while(len)
    {
        if(len > MAX_LEN ) // 如果要读取的数据量大于一次最大读出的数据量
        {
            if(bcm2835_i2c_begin()){  // 开始一次假 写  移动寄存器指针                
                    read_addr[0] = reg_addr + regOffset;
                    bcm2835_i2c_write(read_addr,1);
                    bcm2835_i2c_end();
            }else {
                printf("faild here:%s,%d\r\n",__FILE__,__LINE__);
                len = 0;
            }
            if (bcm2835_i2c_begin()){  
                    bcm2835_i2c_read(data + regOffset,MAX_LEN);
                    bcm2835_i2c_end();
                    len -= MAX_LEN;
                    regOffset += MAX_LEN;
            }else {
                printf("faild here:%s,%d\r\n",__FILE__,__LINE__);
                len = 0;
            }

        }else{
            if (bcm2835_i2c_begin()){  // 开始一次假 写  移动寄存器指针                
                read_addr[0] = reg_addr + regOffset;
                bcm2835_i2c_write(read_addr,1);
                bcm2835_i2c_end();
            }else {
                printf("faild here:%s,%d\r\n",__FILE__,__LINE__);
                len = 0;
            }
            if (bcm2835_i2c_begin()){  
                bcm2835_i2c_read(data+regOffset,len);
                bcm2835_i2c_end();
                len = 0;
            }else {
                printf("faild here:%s,%d\r\n",__FILE__,__LINE__);
                len = 0;
            }

        }
    }  
    // printf("%s\r\n",__FUNCTION__); 
    return 0;  

}

int8_t my_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
    int i;
    uint8_t regOffset=0;
    char writeBuffer[MAX_LEN+1] = {0};
    bcm2835_i2c_setSlaveAddress(dev_id);  
  
    while(len)
    {
        if(len > (MAX_LEN -1) ) // 如果要写入的数据量大于一次最大写入的数据量 -1
        {
            writeBuffer[0] = reg_addr + regOffset;
            i=1;
            while(i< MAX_LEN ){
                writeBuffer[i] = data[i-1];
                i++;
            }
            if (bcm2835_i2c_begin()){               
                bcm2835_i2c_write(writeBuffer,MAX_LEN);
                bcm2835_i2c_end();
                len -= (MAX_LEN-1) ;
                regOffset += (MAX_LEN-1) ;
            }else {
                printf("faild here:%s,%d\r\n",__FILE__,__LINE__);
                len = 0;
                
            }
        }else{
                writeBuffer[0] = reg_addr + regOffset;                
                i=1;
                while(i< len + 1 ){
                    writeBuffer[i] = data[i-1];
                    i++;
                }
            if (bcm2835_i2c_begin()){ 
                bcm2835_i2c_write(writeBuffer,len+1);
                bcm2835_i2c_end();
                len = 0;
            }else {
                printf("faild here:%s,%d\r\n",__FILE__,__LINE__);
                len = 0;
            }
        }
    }     
    return 0;
}

void my_delay(uint32_t period)
{
    struct timeval delay;
	delay.tv_sec = 0;
	delay.tv_usec = period * 1000; // 20 ms
	select(0, NULL, NULL, NULL, &delay);
   // printf("%s\r\n",__FUNCTION__);
}



